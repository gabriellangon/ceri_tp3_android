package com.ceri.tp3;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import java.net.URL;

public class MainActivity extends AppCompatActivity {

    RecyclerView recycleView;
    WeatherDbHelper db;
    ItemAdapter adapter;
    Cursor cityCursor;
    SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        recycleView = findViewById(R.id.recycleView);
        swipeRefreshLayout  = findViewById(R.id.swipeRefreshLayout);

        db = new WeatherDbHelper(this);
        populateListView();

        new ItemTouchHelper (new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
               if(viewHolder instanceof ItemAdapter.ItemViewHolder) {
                   final int deletedIndex = viewHolder.getAdapterPosition();
                   cityCursor.moveToPosition(deletedIndex);
                   db.deleteCity(cityCursor);
                   populateListView();
                   //Toast.makeText(getApplicationContext(),deletedIndex+"",Toast.LENGTH_LONG).show();
                }
            }
        }).attachToRecyclerView(recycleView);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               startActivityForResult(new Intent(MainActivity.this,NewCityActivity.class),2);
            }
        });

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                WeatherAsyncTask asyncTask = new WeatherAsyncTask() ;
                asyncTask.execute();
            }
        });

    }

    public void populateListView(){
        cityCursor = db.fetchAllCities();
        adapter = new ItemAdapter(this, cityCursor);
        recycleView.setHasFixedSize(true);
        recycleView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        recycleView.setAdapter(adapter);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        City city =(City)data.getExtras().getParcelable(City.TAG);
        switch (resultCode) {
            case 1:
                db.addCity(city);
                break;
            case 2:
                db.updateCity(city);
                break;
            default:
                break;
        }
        populateListView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            db.deleteALLCitys();
            db.populate();
            populateListView();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class WeatherAsyncTask extends AsyncTask<String, String, String> {

        private String resp=null;
        ProgressDialog progressDialog;
        JSONResponseHandler jsonResponse;

        @Override
        protected String doInBackground(String... params) {
            publishProgress("Sleeping..."); // Calls onProgressUpdate()
            try {
                cityCursor = db.fetchAllCities();
                City city;
                try {
                    cityCursor.moveToPosition(-1);
                    while (cityCursor.moveToNext()) {
                        city=WeatherDbHelper.cursorToCity(cityCursor);
                        URL url = WebServiceUrl.build(city.getName(),city.getCountry());
                        jsonResponse = new JSONResponseHandler(city);
                        //try {
                            jsonResponse.readJsonStream(url.openStream());
                       // }catch (Exception e){

                        //}

                        db.updateCity(city);
                    }
                } finally {
                    cityCursor.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
                resp = e.getMessage();
            }
            return resp;
        }

        @Override
        protected void onPreExecute() {
           /* progressDialog = ProgressDialog.show(CityActivity.this,
                    "Mise à jour",
                    "Veuillez patienter un instant ...");*/
        }

        @Override
        protected void onPostExecute(String result) {
            // execution of result of Long time consuming operation
            swipeRefreshLayout.setRefreshing(false);
           // if(resp.isEmpty()){
                populateListView();
           /* }else{
                Toast.makeText(getApplicationContext(),"Verifiez votre connexion internet",Toast.LENGTH_SHORT).show();
            }*/

        }

        @Override
        protected void onProgressUpdate(String... text) {

        }

    }
}
