package com.ceri.tp3;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class NewCityActivity extends AppCompatActivity {
    EditText editNewName;
    EditText editNewCountry;
    Button AddButton;
    City city=null;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_city);

        editNewName=findViewById(R.id.editNewName);
        editNewCountry=findViewById(R.id.editNewCountry);
        AddButton=findViewById(R.id.AddButton);

        AddButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            if(editNewCountry.getText().toString().isEmpty() || editNewName.getText().toString().isEmpty() ){
                AlertDialog.Builder alert = new AlertDialog.Builder(v.getContext());
                alert.setMessage("Le nom du pays et de la ville doivent être non nive");
                alert.show();
            }else{
                resultAndClose();
            }

            }
        });

    }

    public void resultAndClose(){
        city = new City(editNewName.getText().toString(),editNewCountry.getText().toString());
        Intent intent = new Intent();
        intent.putExtra(City.TAG,city);
        setResult(1,intent);
        finish();
    }

    @Override
    public void onBackPressed()
    {
        resultAndClose();
    }
}
