package com.ceri.tp3;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.net.URL;

public class CityActivity extends AppCompatActivity {
    ImageView imageView;
    TextView nameCity;
    TextView country;
    TextView editTemperature;
    TextView editHumidity;
    TextView editWind;
    TextView editCloudiness;
    TextView editLastUpdate;
    Button updateButton;
    City city=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city);

        imageView =findViewById(R.id.imageView);
        nameCity =findViewById(R.id.nameCity);
        country =findViewById(R.id.country);
        editTemperature =findViewById(R.id.editTemperature);
        editHumidity =findViewById(R.id.editHumidity);
        editWind =findViewById(R.id.editWind);
        editCloudiness =findViewById(R.id.editCloudiness);
        editLastUpdate =findViewById(R.id.editLastUpdate);
        updateButton = findViewById(R.id.updateButton);

        city = (City)getIntent().getExtras().getParcelable(City.TAG);

        updateView();

        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WeatherAsyncTask asyncTask = new WeatherAsyncTask() ;
                asyncTask.execute();
            }
        });

    }

    public void updateView(){

        nameCity.setText(city.getName());
        country.setText(city.getCountry());
        editTemperature.setText(city.getTemperature()+" °C");
        editHumidity.setText(city.getHumidity()+" %");
        editWind.setText(city.getFullWind());
        editCloudiness.setText(city.getCloudiness()+" %");
        editLastUpdate.setText(city.getLastUpdate());

        if (city.getIcon()!=null && !city.getIcon().isEmpty()) {
            imageView.setImageDrawable(getResources().getDrawable(getResources()
                    .getIdentifier("@drawable/"+"icon_" + city.getIcon(), null, getPackageName())));
            imageView.setContentDescription(city.getDescription());
        }
    }

    public void resultAndClose(){
        Intent intent = new Intent();
        intent.putExtra(City.TAG,city);
        setResult(2,intent);
        finish();
    }

    @Override
    public void onBackPressed()
    {
        resultAndClose();
    }

    private class WeatherAsyncTask extends AsyncTask<String, String, String> {

        private String resp=null;
        ProgressDialog progressDialog;
        JSONResponseHandler jsonResponse;

        @Override
        protected String doInBackground(String... params) {
            publishProgress("Sleeping..."); // Calls onProgressUpdate()
            try {
                URL url = WebServiceUrl.build(city.getName(),city.getCountry());
                jsonResponse = new JSONResponseHandler(city);
                jsonResponse.readJsonStream(url.openStream());
            } catch (Exception e) {
                resp = e.getMessage();
                e.printStackTrace();
            }
            return resp;
        }

        @Override
        protected void onPreExecute() {
            progressDialog = ProgressDialog.show(CityActivity.this,
                   "Mise à jour",
                   "Veuillez patienter un instant ...");
        }

        @Override
        protected void onPostExecute(String result) {
            // execution of result of Long time consuming operation
            progressDialog.dismiss();
            //if(resp.isEmpty()){
                city=jsonResponse.getCity();
                updateView();
          /*  }else{
                Toast.makeText(getApplicationContext(),"Verifiez votre connexion internet",Toast.LENGTH_SHORT).show();
            }*/

        }

        @Override
        protected void onProgressUpdate(String... text) {

        }
    }
}
