package com.ceri.tp3;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.ItemViewHolder> {
    private AppCompatActivity context;
    private Cursor cityCursor;

    public ItemAdapter(AppCompatActivity context, Cursor cityCursor) {
        this.context = context;
        this.cityCursor = cityCursor;
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public int position;
        public ImageView image;
        public TextView cityName,countryName,temperature;

        public ItemViewHolder(View itemView) {
            super(itemView);
            image=itemView.findViewById(R.id.imageViewRow);
            cityName=itemView.findViewById(R.id.cName);
            countryName=itemView.findViewById(R.id.cCountry);
            temperature=itemView.findViewById(R.id.temperature);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            cityCursor.moveToPosition(position);
            City city = WeatherDbHelper.cursorToCity(cityCursor);
            Intent intent = new Intent(context,CityActivity.class);
            intent.putExtra(City.TAG,city);
            context.startActivityForResult(intent,1);
        }
    }

    @NonNull
    @Override
    public ItemAdapter.ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row,parent,false);
        ItemViewHolder ivh = new ItemViewHolder(v);
        return ivh;
    }

    @Override
    public void onBindViewHolder(@NonNull ItemAdapter.ItemViewHolder holder, int position) {
        cityCursor.moveToPosition(position);
        City city = WeatherDbHelper.cursorToCity(cityCursor);
        holder.position=position;
        holder.cityName.setText(city.getName());
        holder.countryName.setText(city.getCountry());
        holder.temperature.setText(city.getTemperature());
        if (city.getIcon()!=null && !city.getIcon().isEmpty()) {
            holder.image.setImageDrawable(context.getResources().getDrawable(context.getResources()
                    .getIdentifier("@drawable/"+"icon_" + city.getIcon(), null, context.getPackageName())));
            holder.image.setContentDescription(city.getDescription());
        }
    }

    @Override
    public int getItemCount() {
        return cityCursor.getCount();
    }
}
